let count = 0;
const ifootnoteMatch = /\^\[(.*?)\]/;

function interpolateIFootnotes(text) {
  return text.replace(ifootnoteMatch, function replace(_, title) {
    count += 1;
    return `<sup title="${title}" class="marked-inline-footnote">note-${count}</sup>`;
  });
}

const basePath = '/data/';

window.$docsify = {
  name: 'Syntropize',
  nameLink: '/',
  logo: '../assets/brand/name-min.svg',
  repo: 'true',
  corner: {
    icon: 'gitlab',
    url: 'https://gitlab.com/syntropize/syntropize',
    title: 'Fork me on GitLab',
  },
  alias: {
    '/': '/Synopsis/',
    '/.*/_sidebar.md': '/_sidebar.md',
  },
  basePath,
  relativePath: false,
  loadSidebar: true,
  subMaxLevel: 2,
  auto2top: true,
  coverpage: false,
  pagination: {
    crossChapter: true,
  },
  search: {
    paths: 'auto',
    depth: 6,
  },
  loadFooter: true,
  copyCode: {
    buttonText: 'Copy',
  },
  markdown: {
    renderer: {
      paragraph(text) {
        return marked.Renderer.prototype.paragraph.apply(null, [interpolateIFootnotes(text)]);
      },
      listitem(text) {
        return marked.Renderer.prototype.listitem.apply(null, [interpolateIFootnotes(text)]);
      },
      text(text) {
        return marked.Renderer.prototype.text.apply(null, [interpolateIFootnotes(text)]);
      },
    },
  },
};
