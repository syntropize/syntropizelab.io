import { /* rm, */ cp } from 'fs/promises';
import libPath from 'path';
import copyDocs from './make-docs/index.js';

const { error } = console;

const des = process.argv[2] || '_public/data';

const files = {
  footer: '_footer.md',
  notFound: '_404.md',
};

try {
  // rm(libPath.join(des, docs), { recursive: true, force:true });
  copyDocs(libPath.join(des));
  cp(`./docs/src/${files.footer}`, libPath.join(des, files.footer));
  cp(`./docs/src/${files.notFound}`, libPath.join(des, files.notFound));
}
catch (e) {
  error('Unable to build Syntropize docs for website');
  error(e);
  process.exit(1);
}
