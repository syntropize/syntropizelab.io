/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
import fs from 'fs/promises';
import libPath from 'path';

import config from './config.js';

const { log, error } = console;

const {
  docs: {
    filenames,
    title,
    contentReducer,
    sidebarReducer,
    sidebarInit,
    maxDepth,
    minDepth,
    mergeTitle: sidebarMergeTitle,
  },
  extra,
} = config;

const noCompactDepth = maxDepth - minDepth;

async function readDirectory(src, path) {
  // 10. Read path entries
  let entries;
  try {
    entries = await fs.readdir(libPath.join(src, path));
  }
  catch (e) {
    if (path === '.') {
      error('Error: Unable to read root path itsef!');
      throw e;
    }
    log(e);
    return {};
  }

  // 20. Read content file
  let content;
  if (entries.includes(filenames.src.content)) {
    try {
      content = await fs.readFile(libPath.join(src, path, filenames.src.content), 'utf8');
    }
    catch (e) {
      if (path === '.') {
        error('Error: Unable to read content file at root path itsef!');
        throw e;
      }
      log(e);
      return {};
    }
  }
  else {
    if (path === '.') {
      throw new Error('Error: No content file at source');
    }
    return {};
  }

  // 30. Read sidebar file (if exists)
  let links;
  if (entries.includes(filenames.src.listing)) {
    // Get Links
    try {
      links = JSON.parse(await fs.readFile(libPath.join(src, path, filenames.src.listing)));
      links = links.filter(link => entries.includes(link));
      if (links.length === 0) {
        return { content };
      }
      return { content, links };
    }
    catch (e) {
      log('Error: Unable to read links for sidebar');
      log(e);
      return { content };
    }
  }
  else {
    return { content };
  }
}

async function writeDirectory(des, path, content) {
  try {
    await fs.mkdir(libPath.join(des, path), { recursive: true });
  }
  catch (e) {
    if (e.code !== 'EEXIST') {
      error('Error: Unable to create destination folder');
      error(e);
      throw (e);
    }
  }

  try {
    await fs.writeFile(libPath.join(des, path, filenames.des.content), content);
  }
  catch (e) {
    error('Error: Unable to create write content');
    error(e);
    throw (e);
  }
}

function extractHeading(text) {
  if (!text) {
    return undefined;
  }
  const headingRegex = /^#\s(\w.*?)$/m;
  const headingMatch = text.match(headingRegex);
  return headingMatch === null ? undefined : headingMatch[1].trim();
}

function mergeContent(content, components) {
  return components.reduce(contentReducer, `${content.trimRight()}\n\n`);
}


async function copyDocs(des) {
  async function getComponents(depth, path, links) {
    try {
      return await Promise.all(links.map(
        link => mergeOrCopy(depth - 1, libPath.posix.join(path, link)),
      ));
    }
    catch (e) {
      error(`Failed on Path: ${path}`, e);
      throw (e);
    }
  }

  async function mergeOrCopy(depth = 1, path = '.') {
    const { content, links } = await readDirectory(config.docs.src, path);
    const components = links && links.length > 0 ? await getComponents(depth, path, links) : [];
    if (components.length === 0) {
      return { content };
    }
    else if (depth <= 0 || (
      depth < noCompactDepth && components.every(c => !Object.prototype.hasOwnProperty.call(c, 'sidebar'))
    )) {
      return {
        content: mergeContent(content, components.map(c => c.content)),
        sidebar: sidebarInit,
      };
    }
    else {
      try {
        await Promise.all(
          components.map((c, i) => writeDirectory(des, libPath.join(path, links[i]), c.content)),
        );
      }
      catch (e) {
        log(`Failed to write components: ${path}`);
        throw (e);
      }

      const sidebar = components.map((component, i) => ({
        link: links[i],
        heading: extractHeading(component.content) || decodeURI(links[i]),
        sidebar: component.sidebar || sidebarInit,
      }))
        .reduce(sidebarReducer, sidebarInit)
      ;

      return { content, sidebar };
    }
  }

  const { sidebar } = await mergeOrCopy(maxDepth);
  return sidebarMergeTitle(sidebar || '', title);
}

async function copyExtra(des) {
  return await Promise.all(extra.map(async function generateSidebarForGroup(group) {
    const sidebar = await Promise.all(group.list.map(async function generateEntryForFile(file) {
      try {
        const text = await fs.readFile(libPath.join(group.src, file), 'utf8');
        const modifiedText = group.replacements.reduce(
          (mod, { input, output }) => mod.replace(input, output),
          text,
        );
        await fs.writeFile(libPath.join(des, file), modifiedText);
        return {
          heading: extractHeading(text),
          link: file,
        };
      }
      catch (e) {
        log(e);
        return null;
      }
    }));
    return group.mergeTitle(sidebar.reduce(group.reducer, group.init), group.title);
  }));
}

async function makeDocsForWebsite(des = config.des) {
  await fs.mkdir(des, { recursive: true });
  await fs.writeFile(
    libPath.join(des, config.sidebar.file),
    config.output(await copyDocs(des), await copyExtra(des)),
  );
}

export default makeDocsForWebsite;
