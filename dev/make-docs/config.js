/*!
 *  Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
const repoUrl = 'https://gitlab.com/syntropize/syntropize-test/-/tree/main';

function demote(markdown) {
  return markdown
    .replace(/^#+ ?/mg, '#$&')
    .replace(/^#{7,} ?(.*)/mg, '**$1**')
  ;
}

const defaults = {
  des: './public',
  sidebar: {
    file: '_sidebar.md',
  },
  docs: {
    src: '_site-data/packages/data/src/Syntropize',
    title: undefined,
    maxDepth: 2,
    minDepth: 0,
    filenames: {
      src: {
        content: '_section.md',
        listing: '_meta.json',
      },
      des: {
        content: 'README.md',
      },
    },
    contentReducer(partialContent, content) {
      return `${partialContent}${(demote(content)).trimRight()}\n\n`;
    },
    sidebarReducer(partialSidebar, { link, heading, sidebar }) {
      return `${partialSidebar}
- [${heading || link}](/${encodeURIComponent(link)}/)${sidebar.replace(/\n/mg, '\n  ').replace(/\(\//mg, `(/${encodeURIComponent(link)}/`)}`
      ;
    },
    mergeTitle(sidebar, title) {
      return title ? `\n**${title}**\n${sidebar}\n` : `${sidebar}\n`;
    },
    sidebarInit: '',
  },
  extra: [{
    src: './_site-data/docs',
    title: 'For Developers',
    list: ['CONTRIBUTING.md', 'USAGE.md'],
    reducer(partialSidebar, { link, heading }) {
      return `${partialSidebar}
- [${heading || link}](/${encodeURIComponent(link)})`;
    },
    mergeTitle(sidebar, title) {
      return `\n**${title}**${sidebar}\n`;
    },
    init: '',
    replacements: [
      {
        input: /\]\(\.\.\//mg,
        output: `](${repoUrl}/`,
      },
      {
        input: /\]\(\.\//mg,
        output: `](${repoUrl}/docs/`,
      },
    ],
  }],
  output(docs, extra = []) {
    return [docs, ...extra].join('\n---\n');
  },
};

export default defaults;
