import subfont from 'subfont';
// import replace from 'replace-in-file';


import libPath from 'path';
import { rm, readdir } from 'fs/promises';

const { error } = console;


async function removeFontFiles(loc) {
  await rm(libPath.join(loc, 'assets', 'fonts'), { recursive: true, force: true });
  const files = await readdir(loc);
  await files.filter(f => f.startsWith('fonts.')).map(f => rm(libPath.join(loc, f)));
}

async function optimize(loc) {
  try {
    await subfont({
      inPlace: true,
      inputFiles: [libPath.join(loc, 'index.html')],
      recursive: false,
      fallbacks: false,
    });

    // await replace({
    //   files: libPath.join(loc, 'index.html'),
    //   from: /<link.*?(href=".*?subfont.*?").*?>/g,
    //   to: '<link $1 rel="preload" as="style" crossorigin>$&',
    // });

    await removeFontFiles(loc);
  }
  catch (e) {
    error('Failed to optimize build!');
    error(e);
    process.exit(1);
  }
}

optimize(process.argv[2] || '_public');
