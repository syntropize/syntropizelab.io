/*
  Access-Control-Allow-Origin: *
  Vary: Origin
  Access-Control-Allow-Methods: GET, HEAD, OPTIONS
  Access-Control-Allow-Headers: Content-Type, Authorization, Content-Length, X-Requested-With
  Cross-Origin-Resource-Policy: same-origin
  Cross-Origin-Opener-Policy: same-origin
  Cross-Origin-Embedder-Policy: require-corp; report-to='default'
  Strict-Transport-Security: max-age=63072000; includeSubDomains; preload
  X-Frame-Options: DENY
  X-Content-Type-Options: nosniff
  Referrer-Policy: no-referrer
  Permissions-Policy: accelerometer=(), ambient-light-sensor=(), autoplay=(), battery=(), camera=(), cross-origin-isolated=(), display-capture=(), document-domain=(), encrypted-media=(), execution-while-not-rendered=(), execution-while-out-of-viewport=(), fullscreen=(self), geolocation=(), gyroscope=(), keyboard-map=(), magnetometer=(), microphone=(), midi=(), navigation-override=(), payment=(), picture-in-picture=(), publickey-credentials-get=(), screen-wake-lock=(), sync-xhr=(), usb=(), web-share=(), xr-spatial-tracking=(), clipboard-read=(), clipboard-write=(self), gamepad=(), speaker-selection=(), conversion-measurement=(), focus-without-user-activation=(), hid=(), idle-detection=(), interest-cohort=(), serial=(), sync-script=(), trust-token-redemption=(), window-placement=(), vertical-scroll=(self)
  NEL: {"report_to":"default","max_age":31536000,"include_subdomains":true}
  Report-To: {"group":"default","max_age":31536000,"endpoints":[{"url":"https://cxres.report-uri.com/a/d/g"}],"include_subdomains":true}, {"group":"csp","max_age":31536000,"endpoints":[{"url":"https://cxres.report-uri.com/r/d/csp/enforce"}],"include_subdomains":true}
  Reporting-Endpoints: default="https://cxres.report-uri.com/a/d/g", csp="https://cxres.report-uri.com/r/d/csp/enforce"
#   Content-Security-Policy: trusted-types typed; require-trusted-types-for 'script';

/
  ! Cross-Origin-Opener-Policy
  ! Referrer-Policy
  Cross-Origin-Opener-Policy: same-origin-allow-popups
  Referrer-Policy: strict-origin-when-cross-origin
  Content-Security-Policy: prefetch-src 'self'; script-src https://cdn.jsdelivr.net 'self' 'sha256-k3jMtXjHJOjTvY7mujJmUljSc2HAJCEyx1rnYOc5dEw='; style-src 'sha256-47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=' 'self'; img-src data: 'self' static.addtoany.com; font-src data: 'self';  manifest-src 'self'; connect-src 'none'; default-src 'none'; base-uri 'self'; worker-src https://*.widgetbot.io/; frame-src https://*.widgetbot.io/; frame-ancestors 'none'; report-uri https://cxres.report-uri.com/r/d/csp/enforce; report-to 'csp';
  Cache-Control: no-cache

/docs
  Content-Security-Policy: prefetch-src 'self'; script-src 'self'; style-src 'self' 'unsafe-hashes' 'sha256-47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=' 'sha256-cFQ8GI90WgYL634cDMk5JZ5o59tNQ/NLjP/DWEvMDx4=' 'sha256-47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=' 'sha256-cmaU5JeQV13CV4PA8T6pWTDIvjGqksO7J8+7Zx76YpU=' 'sha256-6h52w3SLKvExCLgdk907pJwQu8hZtVM4QMirrrfKGaI=' 'sha256-2moxXldo92c2ragmsGXZ9N/ix/3dLAZQHw02FFSEV2s='; img-src data: 'self'; font-src 'self';  manifest-src 'self'; connect-src 'self'; default-src 'none'; base-uri 'self'; frame-src 'none'; frame-ancestors 'none'; report-uri https://cxres.report-uri.com/r/d/csp/enforce; report-to 'csp';
  Cache-Control: no-cache

/*.css
  Cache-Control: public, max-age=31536000

/subfont/*
  Cache-Control: public, max-age=31536000

/NOTICE
  Content-Type: text/plain
  Cache-Control: public, max-age=86400
  X-Robots-Tag: noindex noarchive

/LICEN*
  Content-Type: text/plain
  Cache-Control: public, max-age=86400
  X-Robots-Tag: noindex noarchive

/*.js
  Cache-Control: public, max-age=31536000
  X-Robots-Tag: noindex noarchive

/assets/*
  Cache-Control: public, max-age=31536000, no-transform, immutable
  X-Robots-Tag: noindex noarchive
