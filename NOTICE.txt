Syntropize Website

Copyright (c) 2022, Rahul Gupta and Syntropize Contributors.


The Syntropize website is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License. You should have received a copy of the license along with this work. If not,
visit http://creativecommons.org/licenses/by-sa/4.0/.

SPDX-License-Identifier: CC-BY-SA-4.0


The Syntropize website is based on the Agency theme <https://startbootstrap.com/theme/agency> by
Start Bootstrap <https://startbootstrap.com/>. Start Bootstrap is created and maintained by David
Miller <https://davidmiller.io/>. Code is released under the MIT license.

SPDX-License-Identifier: MIT


Background image is courtesy Unsplash <http://unsplash.com>, shot by Vadim Sherbakov
<http://madebyvadim.com/> and released under the Unsplash License <https://unsplash.com/license>.

Syntropize logo is based on the Polya Font by Adrian Coquet <https://www.behance.net/coquet_adrien>.

Mix animation is modified from original <https://tenor.com/view/mixer-mixing-electric-mixer-mix-cute-gif-17445288>
by CoreyTalmage <https://tenor.com/users/coreytalmage> hosted at Tenor <tenor.com>.
Match animation is modified from original <https://tenor.com/view/match-perfect-gif-11675206>
hosted at Tenor <tenor.com>.


We wish to express our heartfelt gratitude to the incredible community of open source developers
for it is your tireless work that even makes this project possible. You are awesome!
